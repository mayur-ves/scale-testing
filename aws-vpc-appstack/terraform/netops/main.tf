terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
    volterra = {
      source = "volterraedge/volterra"
    }
  }
  required_version = ">= 1.3"
}

# initialize providers
provider "volterra" {
  url           = var.api_url
  api_p12_file  = var.api_p12_file 
}

provider "aws" {
  region      = var.aws_region
  access_key  = var.aws_access_key
  secret_key  = var.aws_secret_key
}

# Initializing data sources
data "volterra_namespace" "system" {
  name = "system"
}

data "volterra_namespace" "shared" {
  name = "shared"
}

# Create k8s cluster role binding for all subjects (users)
resource "volterra_k8s_cluster_role_binding" "admin_cluster_role_binding" {
  name        = format("%s-cluster-role-binding", var.name)
  namespace   = data.volterra_namespace.system.name
  description = format("K8s cluster role binding for %s", var.name)

  k8s_cluster_role {
    name      = "ves-io-admin-cluster-role"
    namespace = data.volterra_namespace.shared.name
    tenant    = "ves-io"
  }

  dynamic "subjects" {
    for_each = var.cluster_role_binding_subjects
    content {
      user  = subjects.value
    }
  }
}

# Create k8s cluster
resource "volterra_k8s_cluster" "appstack_cluster" {
  name        = format("%s-cluster", var.name)
  namespace   = data.volterra_namespace.system.name
  description = format("K8s cluster for %s", var.name)

  local_access_config {
    local_domain = format("%s.local", var.name)
    default_port = true
  }

  use_custom_cluster_role_list {
    cluster_roles {
      name      = "ves-io-admin-cluster-role"
      namespace = data.volterra_namespace.shared.name
      tenant    = "ves-io"
    }
  }

  use_custom_cluster_role_bindings {
    cluster_role_bindings {
      name      = volterra_k8s_cluster_role_binding.admin_cluster_role_binding.name
      namespace = data.volterra_namespace.system.name
      tenant    = data.volterra_namespace.system.tenant_name
    }
  }

  cluster_wide_app_list {
    cluster_wide_apps {
      metrics_server {
        generated_yaml = ""
      }
    }
  }

  global_access_enable          = true
  use_default_psp               = true
  no_insecure_registries        = true
  cluster_scoped_access_deny    = true
}

# create aws creds
resource "volterra_cloud_credentials" "aws_creds" {
  name      = format("%s-creds", var.name)
  namespace = data.volterra_namespace.system.name
  aws_secret_key {
    access_key = var.aws_access_key
    secret_key {
      clear_secret_info {
        url = "string:///${base64encode(var.aws_secret_key)}"
      }
    }
  }
}

# create aws vpc appstack site
resource "volterra_aws_vpc_site" "appstack_site" {
  name          = var.name
  namespace     = data.volterra_namespace.system.name
  description   = format("aws vpc appstack site for scale testing")
  aws_region    = var.aws_region
  instance_type = var.aws_instance_type
  disk_size     = 0
  ssh_key       = var.ssh_key

  aws_cred {
    name      = volterra_cloud_credentials.aws_creds.name
    namespace = data.volterra_namespace.system.name
  }

  vpc {
    new_vpc {
      autogenerate  = true
      primary_ipv4  = var.aws_vpc_cidr
      allocate_ipv6 = false
    }
  }

  voltstack_cluster {
    aws_certified_hw  = var.aws_certified_hw
    
    az_nodes {
      aws_az_name = format("%sa", var.aws_region)
      local_subnet {
        subnet_param {
          ipv4 = cidrsubnet(var.aws_vpc_cidr, 8, 10)
        }
      }
    }

    az_nodes {
      aws_az_name = format("%sb", var.aws_region)
      local_subnet {
        subnet_param {
          ipv4 = cidrsubnet(var.aws_vpc_cidr, 8, 20)
        }
      }
    }

    az_nodes {
      aws_az_name = format("%sc", var.aws_region)
      local_subnet {
        subnet_param {
          ipv4 = cidrsubnet(var.aws_vpc_cidr, 8, 30)
        }
      }
    }

    k8s_cluster {
      name      = volterra_k8s_cluster.appstack_cluster.name
      namespace = data.volterra_namespace.system.name
      tenant    = data.volterra_namespace.system.tenant_name
    }

    no_network_policy         = true
    no_forward_proxy          = true
    no_outside_static_routes  = true
    no_global_network         = true
    no_dc_cluster_group       = true
    sm_connection_public_ip   = true
    default_storage           = true

    allowed_vip_port {
      disable_allowed_vip_port  = true
    }
  }

  logs_streaming_disabled     = true
  total_nodes                 = var.worker_node_count
  default_blocked_services    = true
  direct_connect_disabled     = true
  disable_internet_vip        = true
  egress_gateway_default      = true
  f5xc_security_group         = true

  offline_survivability_mode {
    no_offline_survivability_mode = true
  }

  tags = {
    "deployment"  = var.name
    "TTL"         = -1
  }

  lifecycle {
    ignore_changes = [ labels, description, os, sw ]
  }
}

resource "volterra_tf_params_action" "apply_site" {
  depends_on = [ 
    volterra_aws_vpc_site.appstack_site,
    volterra_cloud_credentials.aws_creds,
    volterra_k8s_cluster.appstack_cluster
  ]
  site_name         = var.name
  site_kind         = "aws_vpc_site"
  action            = "apply"
  wait_for_action   = true
}