variable "api_p12_file" {
  default = ""
}

variable "api_url" {
  default = "https://acmecorp.staging.volterra.us"
}

variable "aws_region" {
  default = "us-west-2"
}

variable "aws_access_key" {
  default = ""
}

variable "aws_secret_key" {
  default = ""
}

variable "name" {
  default = "aws-vpc-appstack-scale"
}

variable "worker_node_count" {
  default = 10
}

variable "aws_vpc_cidr" {
  default = "192.168.0.0/16"
}

variable "aws_certified_hw" {
  default = "aws-byol-voltstack-combo"
}

variable "cluster_role_binding_subjects" {
  default = ["ma.sinha@f5.com", "mayur@ves.io"]
}

variable "aws_instance_type" {
  default = "t3.xlarge"
}

variable "ssh_key" {
  default = ""
}