output "aws_vpc_id" {
  value = data.aws_vpc.vpc.id
}

output "clinet_vm_public_ip" {
  value = module.client_vm.public_ip
}

output "appstack_kubeconfig_b64" {
  value = local.appstack_kubeconfig_b64
}