terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
    volterra = {
      source = "volterraedge/volterra"
    }
    http-full = {
      source = "salrashid123/http-full"
      version = "1.2.8"
    }
    time = {
      source = "hashicorp/time"
      version = "0.7.2"
    }
  }
  required_version = ">= 1.3"
}

# initialize providers
provider "volterra" {
  url           = var.api_url
  api_p12_file  = var.api_p12_file 
}

provider "aws" {
  region      = var.aws_region
  access_key  = var.aws_access_key
  secret_key  = var.aws_secret_key
}

provider "http-full" {}

provider "time" {}

# Initializing data sources
data "volterra_namespace" "system" {
  name = "system"
}

data "volterra_namespace" "shared" {
  name = "shared"
}

# Local variables
locals {
  appstack_kubeconfig_b64 = base64encode(data.http.appstack_kubeconfig.body)
}

resource "volterra_namespace" "ns" {
  name        = var.name
  description = format("Namespace for %s related objects", var.name)

  provisioner "local-exec" {
    command = "sleep 5"
  }
}

# Get 180 days expiration timestamp
resource "time_offset" "exp_time" {
  # triggers = {
  #   "always_run" = timestamp()
  # }
  offset_days = 180
}

# Get global kubeconfig
data "http" "appstack_kubeconfig" {
  provider  = http-full

  url       = format("%s/web/namespaces/system/sites/%s/global-kubeconfigs", var.api_url, var.name)
  method    = "POST"
  request_headers = {
    Content-Type  = "application/json"
    Authorization = format("APIToken %s", var.api_token)
  }
  request_body    = jsonencode({ expiration_timestamp: time_offset.exp_time.rfc3339, site: var.name })
}

# Create appstack site service discovery
resource "volterra_discovery" "appstack_service_discovery" {
  name        = format("nginx-on-%s", var.name)
  namespace   = data.volterra_namespace.system.name
  description = format("Service discovery object to discover nginx-200 service on %s", var.name)

  where {
    site {
      ref {
        name      = var.name
        namespace = data.volterra_namespace.system.name
        tenant    = data.volterra_namespace.system.tenant_name
      }
      network_type          = "VIRTUAL_NETWORK_SITE_LOCAL"
      disable_internet_vip  = true
    }
  }

  discovery_k8s {
    access_info {
      kubeconfig_url {
        secret_encoding_type = "EncodingNone"
        clear_secret_info {
          url = format("string:///%s", local.appstack_kubeconfig_b64)
        }
      }
      reachable = true
    }
    publish_info {
      disable = true
    }
  }
}

resource "volterra_origin_pool" "nginx_pool" {
  name                    = format("nginx-200-on-%s", var.name)
  namespace               = volterra_namespace.ns.name
  description             = format("Origin pool for nginx-200 service on %s", var.name)
  loadbalancer_algorithm  = "ROUND ROBIN"
  endpoint_selection      = "LOCAL_PREFERRED"
  port                    = 8080
  no_tls                  = true

  origin_servers {
		k8s_service {
      service_name  = "nginx-200-svc.acmecorp"
      service_selector {
        expressions = []
      }
      vk8s_networks = true
      site_locator {
        site {
          name      = var.name
          namespace = data.volterra_namespace.system.name
          tenant    = data.volterra_namespace.system.tenant_name
        }
      } 
    }
  }
}

resource "volterra_http_loadbalancer" "nginx_http_lb" {
  name                            = format("nginx-200-on-%s", var.name)
  namespace                       = volterra_namespace.ns.name
  description                     = format("HTTP LB for nginx-200 service on %s", var.name)
  domains                         = ["nginx-200.local"]
  
  http {
    port  = 80
  }

  advertise_custom {
    advertise_where {
      site {
        network = "SITE_NETWORK_OUTSIDE"
        site {
          name      = var.name
          namespace = data.volterra_namespace.system.name
          tenant    = data.volterra_namespace.system.tenant_name
        }
      }
    }
  }

  default_route_pools {
    cluster {
      name  = var.name
    }
    pool {
      name      = volterra_origin_pool.nginx_pool.name
      namespace = volterra_namespace.ns.name
      tenant    = volterra_namespace.ns.tenant_name
    }
    weight =  1
    priority =  1
  }
}
