data "aws_vpc" "vpc" {
  filter {
    name    = "tag:ves-io-site-name"
    values  = [var.name]
  }
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

data "aws_security_group" "aws_sg" {
  filter {
    name    = "vpc-id"
    values  = [data.aws_vpc.vpc.id]
  }

  filter {
    name    = "tag:ves-io-site-name"
    values  = [var.name]
  }
}

resource "aws_key_pair" "ssh_key" {
  key_name    = format("%s-key", var.name)
  public_key  = var.ssh_key
}

data "aws_subnet" "subnet" {
  availability_zone = format("%sa", var.aws_region)
  vpc_id            = data.aws_vpc.vpc.id
}

module "client_vm" {
  source      = "terraform-aws-modules/ec2-instance/aws"
  version     = "5.0.0"

  name                        = format("%s-client", var.name)
  ami                         = data.aws_ami.ubuntu.id
  instance_type               = var.aws_instance_type
  key_name                    = aws_key_pair.ssh_key.key_name
  monitoring                  = false
  vpc_security_group_ids      = [data.aws_security_group.aws_sg.id]
  subnet_id                   = data.aws_subnet.subnet.id
  associate_public_ip_address = true

  tags  = {
    "deployment"  = var.name
    "TTL"         = -1
  }

  user_data               = <<-EOT
    #!/bin/bash

    hostnamectl set-hostname ${var.name}
    apt update && apt install -y curl make cmake unzip
    cd /home/ubuntu && git clone https://github.com/wg/wrk.git
    cd wrk && make

  EOT
}